import React from 'react';
import HomePage from './pages/HomePage';
import Header from './components/Header';
import SectionTwo from './pages/SectionTwo';
import SectionThree from './pages/SectionThree';
import SectionFour from './pages/SectionFour';
import SectionFive from './pages/SectionFive';
import SectionSix from "./pages/SectionSix";

import './app.scss';


function App() {
    return (
        <div className="app">
            <Header/>
            <HomePage/>
            <SectionTwo/>
            <SectionThree/>
            <SectionFour/>
            <SectionFive/>
            <SectionSix/>
        </div>
    );
}

export default App;
