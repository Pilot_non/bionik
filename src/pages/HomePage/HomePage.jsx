import React from 'react';
import Title from './components/Title';
import Side from './components/Side'

import './HomePage.scss';


function HomePage() {

    return (
        <div className='homepage'>
            <div className='section_1'>
                <Title/>
                <Side/>
            </div>
        </div>
    )
}


export default HomePage;