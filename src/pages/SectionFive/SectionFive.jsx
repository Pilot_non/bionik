import React from 'react';
import './SectionFive.scss';
import Img from '../../assets/img/imgSectionFive.png'

function SectionFive() {
    return(
        <div className='sectionFive'>
            <div className='sectionFifth'>
                <div className='sectionFiveLine'></div>
                <div className='philosophy'>
                    <h1 className='philosophyTitle'>Філософія</h1>
                    <p className='philosophyDescription'>
                        Природа неймовірна, вона створює цілі<br/>
                        “світи” — екосистеми зі своїм життям.<br/>
                        Саме такую концепцію ми втілили у БЦ Bionic<br/>
                        — кожна з 7 секцій має власний, унікальний<br/>
                        інтер’єр, присвячений природнім<br/>
                        екосистемам
                        </p>
                </div>
                <div className='imgSectionFive'>
                    <img src={Img}></img>
                </div>
            </div>

        </div>
    )
}

export default SectionFive;