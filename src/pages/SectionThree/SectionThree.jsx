import React from 'react';


import './SectionThree.scss';

function SectionThree() {
    return (
        <div className='sectionThree'>
            <div className='thirdSection'>
                <div className='lineSectionThree'></div>
                <div className='advantageBionic'>
                    <p>Переваги БЦ Bionic</p>
                </div>
                <div className='firstBlock'>
                    <div className='blockOne'>
                        <h1 className='titleBlockSectionThree'>Стильна архітектура</h1>
                        <p className='descriptionBlockSectionThree'>Поруч ТРЦ, Епіцентр, Ашан, <br/>
                            кав'ярні та транспортна розв'язка)
                        </p>
                        <p className='numberSectionThree'>
                            #01
                        </p>
                    </div>
                    <div className='block'>
                        <h1 className='titleBlockSectionThree'>Зелена зона</h1>
                        <p className='descriptionBlockSectionThree'>На території БЦ висаджені дерева<br/>
                            та є сквери і зони відпочинку
                        </p>
                        <p className='numberSectionThree'>
                            #02
                        </p>
                    </div>
                    <div className='block'>
                        <h1 className='titleBlockSectionThree'>Трансфер</h1>
                        <p className='descriptionBlockSectionThree'>До метро Васильковська (~15 хв),<br/>
                            вранці та ввечері
                        </p>
                        <p className='numberSectionThree'>
                            #03
                        </p>
                    </div>
                    <div className='block'>
                        <h1 className='titleBlockSectionThree'>Розташування</h1>
                        <p className='descriptionBlockSectionThree'>Голосіївський район міста Києева - <br/>
                            поруч з центром
                        </p>
                        <p className='numberSectionThree'>
                            #04
                        </p>
                    </div>
                </div>
                <div className='secondBlock'>
                    <div className='block'>
                        <h1 className='titleBlockSectionThree'>
                            Інфраструктура
                        </h1>
                        <p className='descriptionBlockSectionThree'>
                            Поруч ТРЦ, Епіцентр, Ашан,<br/>
                            кав'ярні та транспортна розв'язка)
                        </p>
                        <p className='numberSectionThree'>
                            #05
                        </p>
                    </div>
                    <div className='block'>
                        <h1 className='titleBlockSectionThree'>
                            Склад на території
                        </h1>
                        <p className='descriptionBlockSectionThree'>
                            Зручне рішення для <br/>
                            дистриб’юторів та <br/>
                            інтернет-магазинів
                        </p>
                        <p className='numberSectionThree'>
                            #06
                        </p>
                    </div>
                    <div className='block'>
                        <h1 className='titleBlockSectionThree'>
                            Охорона 24/7
                        </h1>
                        <p className='descriptionBlockSectionThree'>
                            Пропускна система на вході,<br/>
                            закрита територія та відеонагляд
                        </p>
                        <p className='numberSectionThree'>
                            #07
                        </p>
                    </div>
                    <div className='block'>
                        <h1 className='titleBlockSectionThree'>
                            Паркінг
                        </h1>
                        <p className='descriptionBlockSectionThree'>
                            Підземний та наземний паркінги <br/>
                            загальною кількістю 500+ місць
                        </p>
                        <p className='numberSectionThree'>
                            #08
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SectionThree;