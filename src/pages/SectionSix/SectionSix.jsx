import React from 'react';
import './SectionSix.scss';

import Img1 from '../../assets/img/imgOneBlockOne.png';
import Img2 from '../../assets/img/imgTwoBlockOne.png';

function SectionSix() {
    return(
        <div className='sectionSix'>
            <div className='sectionSixth'>
                <div className='sectionSixLine'></div>
                <h1 className='infrastructure'>Інфраструктура</h1>
                <div className='sectionSixBlockOne'>
                    <div className='cafeAndRestaurant'>
                        <img  src={Img1}></img>
                        <p className='pBlockOne'>Кафе та ресторани</p>
                        <div className='lineBlockOne'></div>
                    </div>
                    <div className='banksBlockOne'>
                        <img src={Img2}></img>
                        <p className='pBlockOne'>Банки</p>
                        <div className='lineBlockOne'></div>
                    </div>
                    <div className='textBlockOne'>
                        <p>
                            Природа неймовірна,<br/>
                            вона створює цілі<br/>
                            “світи” — екосистеми<br/>
                            зі своїм життям.
                        </p>
                    </div>
                </div>
                <div className='sectionSixBlockTwo'>

                </div>
            </div>
        </div>
    )
}

export default SectionSix;