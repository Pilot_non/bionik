import React from 'react';
import logo from '../../files/logo.png'
import HandCursor from '../../files/hand-cursor.png';

import './Header.css'

function Header() {
    return (
        <div className='header'>
            <img src={logo} className='logo'/>
            <div className='navi'>
                <div className='menu-item'>
                    <p className='p'>Про бізнес центр</p>
                    <div className='line'/>
                </div>
                <div className='menu-item'>
                    <p className='p'>Інфраструктура</p>
                </div>
                <div className='menu-item'>
                    <p className='p'>Планування</p>
                </div>
                <div className='menu-item'>
                    <p className='p'>Умови придбання</p>
                </div>
                <div className='menu-item'>
                    <p className='p'>Забудовник</p>
                </div>
                <div className='menu-item'>
                    <p className='p'>Новини</p>
                </div>
                <div className='menu-item'>
                    <p className='p'>Контакти</p>
                </div>
            </div>

        </div>
    )
}

export default Header;